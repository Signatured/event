package me.signatured.eventwarp;

import java.util.ArrayList;

import org.bukkit.Location;

public class Warp {
	
	public static ArrayList<Warp> warpObects = new ArrayList<Warp>();
	
	private Location loc;
	private int uses;
	private boolean isBroadcasting = true;
	
	public Warp(Location loc, int uses) {
		this.loc = loc;
		this.uses = uses;
		
		warpObects.add(this);
	}
	
	public Location getLoc() {
		return this.loc;
	}
	
	public void setLoc(Location loc) {
		this.loc = loc;
	}
	
	public int getUses() {
		return this.uses;
	}
	
	public void setUses(int uses) {
		this.uses = uses;
	}
	
	public boolean getBroadcasting() {
		return this.isBroadcasting;
	}
	
	public void setBroadcasting(boolean b) {
		this.isBroadcasting = b;
	}
}
