package me.signatured.eventwarp;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

public class BroadcastTask extends BukkitRunnable {

	@Override
	public void run() {
		if (WarpManager.getManager().getWarp() != null) {
			if (WarpManager.getManager().getWarp().getBroadcasting()) {
				if (WarpManager.getManager().getWarp().getUses() == -1) {
					Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes
							('&', "&e&l[Event] &aAn event is currently in progress! Type /event to be teleported to the event! You have unlimited warp uses!"));
				} else {
					Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes
							('&', "&e&l[Event] &aAn event is currently in progress! Type /event to be teleported to the event! You will get " + WarpManager.getManager().getWarp().getUses() + " warp(s) to the event!"));
				}
			}
		}
	}
}
