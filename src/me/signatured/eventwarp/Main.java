package me.signatured.eventwarp;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	@SuppressWarnings("deprecation")
	public void onEnable() {
		Bukkit.getScheduler().runTaskTimer(this, new BroadcastTask(), 10, 20 * 60);
		
		getCommand("eventset").setExecutor(new CommandManager());
		getCommand("eventdelete").setExecutor(new CommandManager());
		getCommand("eventbroadcast").setExecutor(new CommandManager());
		getCommand("event").setExecutor(new CommandManager());
		
		Bukkit.getPluginManager().registerEvents(new Listeners(), this);
		
		new WarpManager();
	}
	
	public void onDisable() {
		for (EventSign sign : EventSign.signObjects) {
			sign.getLoc().getWorld().getBlockAt(sign.getLoc()).setType(Material.AIR);
		}
	}
}
