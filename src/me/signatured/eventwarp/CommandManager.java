package me.signatured.eventwarp;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManager implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("eventset")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				
				if (player.hasPermission("event.set")) {
					if (args.length == 1) {
						try {
							int uses = Integer.parseInt(args[0]);
							
							WarpManager.getManager().createWarp(player.getLocation(), uses);
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aEvent warp set!"));
							
							if (WarpManager.getManager().getWarp().getUses() == -1) {
								Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes
										('&', "&e&l[Event] &aAn event is currently in progress! Type /event to be teleported to the event! You have unlimited warp uses!"));
							} else {
								Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes
										('&', "&e&l[Event] &aAn event is currently in progress! Type /event to be teleported to the event! You will get " + WarpManager.getManager().getWarp().getUses() + " warps to the event!"));
							}
						} catch (Exception e) {
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cUsage: /eventset <uses>"));
						}
					} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cUsage: /eventset <uses>"));
				} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cYou don't have permission to do this!"));
			}
		} else if (cmd.getName().equalsIgnoreCase("event")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				
				WarpManager.getManager().sendLocation(player);
			}
		} else if (cmd.getName().equalsIgnoreCase("eventbroadcast")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				
				if (player.hasPermission("event.broadcast")) {
					if (WarpManager.getManager().getWarp() != null) {
						if (args.length == 0) {
							if (WarpManager.getManager().getWarp().getBroadcasting()) {
								WarpManager.getManager().getWarp().setBroadcasting(false);
								player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aEvent broadcasting disabled!"));
							}
							else {
								WarpManager.getManager().getWarp().setBroadcasting(true);
								player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aEvent broadcasting enabled!"));
							}
						} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cUsage: /eventbroadcast"));
					}  else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cNo event warp currently set!"));
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("eventdelete")) {
			if (sender.hasPermission("event.set")) {
				if (WarpManager.getManager().getWarp() != null) {
					WarpManager.getManager().deleteWarp();
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aEvent warp deleted!"));
				} else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cNo event warp currently set!"));
			}
		}
		return true;
	}
}
