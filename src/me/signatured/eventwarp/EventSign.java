package me.signatured.eventwarp;

import java.util.ArrayList;

import org.bukkit.Location;

public class EventSign {
	
	public static ArrayList<EventSign> signObjects = new ArrayList<EventSign>();
	
	private Location loc;
	
	public EventSign(Location loc) {
		this.loc = loc;
		
		signObjects.add(this);
	}
	
	public Location getLoc() {
		return this.loc;
	}
	
	public void setLoc(Location loc) {
		this.loc = loc;
	}
}
