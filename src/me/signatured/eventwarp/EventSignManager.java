package me.signatured.eventwarp;

import org.bukkit.Location;

public class EventSignManager {
	
	private static EventSignManager wm = new EventSignManager();
	
	public static EventSignManager getManager() {
		return wm;
	}
	
	public EventSign getSign(Location loc) {
		for (EventSign sign : EventSign.signObjects) {
			if (sign.getLoc().equals(loc)) return sign;
		}
		return null;
	}
	
	public void createSign(Location loc) {
		new EventSign(loc);
	}
	
	public void deleteSign(Location loc) {
		if (getSign(loc) != null) {
			EventSign.signObjects.remove(getSign(loc));
		}
	}
}
