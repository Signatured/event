package me.signatured.eventwarp;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

public class Listeners implements Listener {
	
	public ArrayList<String> users = new ArrayList<String>();
	
	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		if (e.getPlayer().isOp()) {

			if (e.getLine(0).equalsIgnoreCase("[Event]")) {

				EventSignManager.getManager().createSign(e.getBlock().getLocation());
				e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aEvent sign created!"));
				
				e.setLine(0, ChatColor.translateAlternateColorCodes('&', "&e&l[Event]"));
				e.setLine(2, ChatColor.translateAlternateColorCodes('&', "&1Click me!"));
			}
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			System.out.println("ran1");
			Block block = e.getClickedBlock(); 
			Player player = e.getPlayer();
			
			if (EventSignManager.getManager().getSign(block.getLocation()) != null) {
				System.out.println("ran2");
				if (WarpManager.getManager().getWarp() != null) {
					System.out.println("ran3");
					users.add(player.getName());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aYou received your event gear!"));
					
					player.getInventory().clear();
					giveItems(player);
					
				} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cNo event found!"));
			}
		}
	}
	
	@EventHandler
	public void onSignBlocks(BlockBreakEvent e) {
		Block block = e.getBlock();
		
		if (EventSignManager.getManager().getSign(block.getLocation()) != null) {
			EventSignManager.getManager().deleteSign(block.getLocation());
			e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aEvent sign deleted!"));
		}
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		
		if (users.contains(player.getName())) {
			clearItems(player);
			users.remove(player.getName());
		}
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player damaged = (Player) e.getEntity();
			
			if (e.getDamage() >= damaged.getHealth()) {
				if (users.contains(damaged.getName())) {
					users.remove(damaged.getName());
					clearItems(damaged);
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player damaged = (Player) e.getEntity();
			
			if (e.getDamage() >= damaged.getHealth()) {
				if (users.contains(damaged.getName())) {
					users.remove(damaged.getName());
					clearItems(damaged);
				}
			}
		}
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		Player player = e.getPlayer();
		
		if (users.contains(player.getName())) e.setCancelled(true);
	}
	
	public void giveItems(Player player) {		
		player.getInventory().setItem(0, new ItemStack(Material.IRON_SWORD));
		player.getInventory().setItem(1, new ItemStack(Material.DIAMOND_AXE));
		player.getInventory().setItem(2, new ItemStack(Material.DIAMOND_PICKAXE));
		player.getInventory().setItem(3, new ItemStack(Material.DIAMOND_HOE));
		player.getInventory().setItem(4, new ItemStack(Material.INK_SACK, 64, (short)11));
		player.getInventory().setItem(5, new ItemStack(Material.PAPER, 64));
		player.getInventory().setItem(6, new ItemStack(Material.SHEARS));
		player.getInventory().setItem(7, new ItemStack(Material.WEB, 64));
		player.getInventory().setItem(8, new ItemStack(Material.WEB, 64));
		
		player.getInventory().setItem(9, new ItemStack(Material.CLAY_BALL, 64));
		player.getInventory().setItem(18, new ItemStack(Material.CLAY_BALL, 64));
		player.getInventory().setItem(27, new ItemStack(Material.CLAY_BALL, 64));
		
		player.getInventory().setItem(10, new ItemStack(Material.FLINT, 64));
		player.getInventory().setItem(11, new ItemStack(Material.FLINT, 64));
		player.getInventory().setItem(19, new ItemStack(Material.FLINT, 64));
		player.getInventory().setItem(20, new ItemStack(Material.FLINT, 64));
		player.getInventory().setItem(28, new ItemStack(Material.FLINT, 64));
		player.getInventory().setItem(29, new ItemStack(Material.FLINT, 64));
		
		player.getInventory().setItem(12, new ItemStack(Material.SEEDS, 64));
		player.getInventory().setItem(21, new ItemStack(Material.SEEDS, 64));
		player.getInventory().setItem(30, new ItemStack(Material.SEEDS, 64));
		
		player.getInventory().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
		player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
		player.getInventory().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
		player.getInventory().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
		
		player.updateInventory();
	}
	
	public void clearItems(Player player) {
		player.getInventory().clear();
		
		player.getInventory().setHelmet(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setLeggings(null);
		player.getInventory().setBoots(null);
	}
}
