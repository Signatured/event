package me.signatured.eventwarp;

import java.util.HashMap;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class WarpManager {
	
	private static WarpManager wm = new WarpManager();
	
	public static WarpManager getManager() {
		return wm;
	}
	
	public HashMap<String, Integer> uses = new HashMap<String, Integer>();
	
	public Warp getWarp() {
		for (Warp w : Warp.warpObects) {
			return w;
		}
		return null;
	}
	
	public void createWarp(Location loc, int uses) {
		if (getWarp() != null) {
			Warp.warpObects.remove(getWarp());
			this.uses.clear();
			
			new Warp(loc, uses);
		} else new Warp(loc, uses);
	}
	
	public void deleteWarp() {
		if (getWarp() != null) {
			Warp.warpObects.remove(getWarp());
		}
	}
	
	public void sendLocation(Player player) {
		if (getWarp() != null) {
			if (!(getWarp().getUses() == -1)) {
				if (checkUses(player) < getWarp().getUses()) {
					int newUses = checkUses(player) + 1;
					int usesLeft = getWarp().getUses() - newUses;
					uses.put(player.getName(), newUses);
					
					player.teleport(getWarp().getLoc());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aTeleported to event! You have " + usesLeft + " event warp uses left!"));
				} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cYou don't have anymore event warps left!"));
			} else {
				player.teleport(getWarp().getLoc());
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &aTeleported to event!"));
			}
		} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l[Event] &cNo event warp is currently set!"));
	}
	
	public int checkUses(Player player) {
		int currentUses;
		
		try {
			currentUses = uses.get(player.getName());
		} catch (Exception e) {
			return 0;
		}
		return currentUses;
	}
}
